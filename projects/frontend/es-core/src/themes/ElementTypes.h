//
// Created by bkg2k on 17/12/23.
//
#pragma once

enum class ThemeElementType
{
  None, //!< Special value
  Image,
  Box,
  Video,
  Text,
  ScrollText,
  TextList,
  Container,
  NinePatch,
  DateTime,
  Rating,
  Sound,
  HelpSystem,
  Carousel,
  MenuBackground,
  MenuIcons,
  MenuSwitch,
  MenuSlider,
  MenuButton,
  MenuText,
  MenuTextSmall,
  MenuSize,
};
